V C62: remove
V R38: -> 0Ω // I used 1Ω is fine too

V R29: -> 1kΩ
V R30: -> 1kΩ

V C58: -> 150nF // Don't have, use 220nF
V C59: -> 150nF
V C66: -> 150nF

V C55: -> 1.5nF // Don't have, use 1nF
V C56: -> 1.5nF

V C61: -> 68nF
V C67: -> 68nF
V C57: -> 68nF
V C65: -> 68nF

V R40: -> 9.1kΩ
V R33: -> 750
V R34: -> 510

R41: -> 1.65k // 3.3k x2
R31: -> 1.65k 

L6: -> 1k
C71: -> 9k1 parallel C71

C56 -> 10nF + 4.7nF

7k5 resistors from GND to diode side of R36 and R44
