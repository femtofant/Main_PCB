#set page(width: 12cm, height: auto, margin: (x: .7cm, top: .7cm, bottom: 12cm))
= TPS54331DR

#let to-string(content) = {
  if content.has("text") {
    content.text
  } else if content.has("children") {
    content.children.map(to-string).join("")
  } else if content.has("body") {
    to-string(content.body)
  } else if content == [ ] {
    " "
  }
}

= Inductor
$ L_("MIN") = (V_("OUT"("MAX"))(V_("IN"("MAX"))-V_("OUT")))/(V_("IN"("MAX"))K_("IND") I_("OUT") F_("SW")) $
$K_("IND")$: Inductor ripple current relative to maximum output current? 0.3 for higher ESR vapacitors, 0.2 for higher ESR capacitors?
\ \

#let u(x, unit) = {
  str(x)+unit
}

#let printParams(x) = {
  [
  $V_("OUT"("MAX"))$ = #x.Voutmax\
  $V_"OUT"$          = #x.Vout\
  $V_"IN"("MAX")$    = #x.Vinmax\
  $K_"IND"$          = #x.Kind\
  $I_"OUT"$          = #x.Iout\
  $F_"SW"$           = #x.Fsw\
  $L_"OUT"$          = #x.Lout μH\
  ]
}

#let Lmin(x) = {
  raw(to-string[(#x.Voutmax\V\(#x.Vinmax\V-#x.Vout\V))/(#x.Vinmax\V\*#x.Kind\*#x.Iout\A\*#x.Fsw\Hz)])
}

= Check of datasheet
#let datasheet_params = (
  Voutmax: 3.33,
  Vout: 3.3,
  Vinmax: 28,
  Kind: .3,
  Iout: 3,
  Fsw: 570000,
  Lout: 6.8, // In μH
)

#printParams(datasheet_params)\
#Lmin(datasheet_params) = 5.726190476 μH

= Our params
#let our_params = (
  Voutmax: 6.03,
  Vout: 6,
  Vinmax: 12,
  Kind: .3,
  Iout: 3,
  Fsw: 570000,
  Lout: 6.8, // In μH
)

#printParams(our_params)\
#Lmin(our_params) = 5.877192982 μH

$ I_L("RMS")^2 = sqrt(I_"OUT"("MAX")^2 + 1/12 ((V_"OUT" (V_"IN"("MAX")-V_"OUT"))/(V_"IN"("MAX") L_"OUT" F_"SW" 0.8))^2) $

#let I_Lrms(x) = {
  raw(to-string[sqrt(#x.Iout^2 + 1/12\*((#x.Vout\*(#x.Vinmax\-#x.Vout))/(#x.Vinmax\*#x.Lout\micro\*#x.Fsw\*0.8))^2)])
}

= Datasheet
#I_Lrms(datasheet_params) = 3.012216375Arms

= Ours
#I_Lrms(our_params) = 3.012972526Arms
